package com.crunchify.restjersey;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/data")
public class DataStorageService {
	   @POST
	    @Path("/post")
	    @Consumes(MediaType.APPLICATION_JSON)
	    public Response createDataInJSON(String data) { 

	        String result1 = "Data received: "+data;
	        System.out.println(result1);
	        String result = data;
	        return Response.status(201).entity(result).build(); 
	   }
}